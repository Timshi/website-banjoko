<?php
    include("sqlconfig.php");

    $servername = "localhost";
    
    $conn = new mysqli($servername, USERNAME, PASSWORD);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $result = $conn->query("SELECT hashtag FROM banjoko.hashtags;");

    $data = [];
    foreach ($result as $row) {
        $value = utf8_encode($row["hashtag"]);
        if (empty($data)) $data[0] = $value;
        else $data[count($data)] = $value;
    }

	echo json_encode($data);

    $conn->close();
?>
<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
		<title>Banjoko is love ♥</title>
		<link rel="stylesheet" href="styles.css">
		<script src="scripts.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	</head>
	<body>
		<?php
			$welcomeText = "Die Seite rund um unsere allseits geliebte flauschige Maine-Coon-Katze des Kronprinzen von Edingen Magusch I. und seiner Gemahlin Franz.\r\nEuch erwarten spannende und interessante Fakten zu unserem geliebten Vierbeiner sowie aktuelle und brandheiße News aus der Katzenwelt in Edingen.";
		?>
	
		<header>
			<img id="header-image" alt="header" src="/img/header0.png">
			<div id="header-text">
			<h1>Banjoko ♥</h1>
				<p><?php echo $welcomeText; ?></p>
			</div>
			<nav></nav>
		</header>
	
		<main>
			<section>
				<h2>Ruhe in Frieden Banjoko</h2>
				<p>Liebe Banjoko,</p>
				<p>wir alle sind immer noch über Deinen plötzlichen und viel zu frühen Tod schockiert.</p>
				<p>Deine unnachahmliche Art Menschen glücklich zu machen hat uns alle sehr beeindruckt.</p>
				<p>Umso schwerer fällt es uns nun Abschied von Dir nehmen zu müssen.</p>
				<p>In den Stunden, in denen vor allem Deine Herrchen unter deiner Abwesenheit leiden, wünschen wir ihnen viel Kraft.</p>
				<br>
				<p>Auf die Frage, ob es einen Katzenhimmel gibt, antwortete Dein Herrchen einst:</p>
				<p><i>"Natürlich gibt es einen Katzenhimmel, in dem es unendlich viele Spielsachen und Kratzbäume gibt."</i></p>
				<br>
				<p>Banjoko, wir alle wünschen uns, dass dies stimmt und du glücklich bist - wo auch immer du gerade bist.</p>
				<p>Für uns bleibst Du für immer unvergessen!</p>
				<br>
				<p><b>† 06.11.2019 Banjoko</b></p>
			</section>
			
			<hr>
		
			<section id="introduction-part">
				<h2>Wilkommjoko ♥</h2>
				<p><?php echo $welcomeText; ?></p>
			</section>
			
			<hr>
		
			<section id="social-media">
				<h2>Socialjoko ♥</h2>
				<p>Du möchtest Beiträge zu Banjoko auf Social Media teilen? Dann nutze einen unserer Hashtags wie z.B. #<?php include("randomhashtag.php"); ?></p>
				<p>Du möchtest den heißesten Scheiß zu Banjoko erfahren?</p>
				<a href="https://www.instagram.com/banjokocat/" target="_blank">Dann folge Banjoko jetzt auf Instagram!</a>
			</section>
			
			<hr>
		
			<section id="ban-jo-ko">
				<h2>Ban-jo-ko ♥</h2>
				<p>Spiele den Spiele-Klassiker Tic-Tac-Toe mit unserem vierbeinigen Freund Banjoko!</p>
				<input id="restart" type="submit" value="Neu starten">
				
				<div class="table">
					<div class="table-row">
						<div id="1" class="table-cell"></div>
						<div id="2" class="table-cell"></div>
						<div id="3" class="table-cell"></div>
					</div>
					<div class="table-row">
						<div id="4" class="table-cell"></div>
						<div id="5" class="table-cell"></div>
						<div id="6" class="table-cell"></div>
					</div>
					<div class="table-row">
						<div id="7" class="table-cell"></div>
						<div id="8" class="table-cell"></div>
						<div id="9" class="table-cell"></div>
					</div>
				</div>
				
				<div id="winner">
					<h3>Winner, winner, Banjoko-Dinner!</h3>
					<p id="winner-text"></p>
					<img src="/img/win.png" alt="win">		
				</div>
				
				<div id="draw">
					<h3>Unentschieden. Laaaaaaaaaaaaaaaaaaaaaaangweilig. Miau.</h3>
					<img src="/img/draw.png" alt="draw">
				</div>
			</section>
			
			<hr>
			
			<section>
				<h2>Galgenjoko ♥</h2>
				<p>Errate die trendigen Hashtags des Trendsetters Magusch aka mpw bei Galgenjoko!</p>								
				<div class="one-line-container">
					<p>Gesuchter Hashtag: #</p>
					<p id="searched-word"></p>				
				</div>
				<div class="one-line-container">
					<p>Übrige Katzenleben:</p>
					<p id="live-counter">7</p>
				</div>
				<div id="failed-guesses-container" class="one-line-container">
					<p>Fehlerhafte Versuche:</p>
					<p id="failed-guesses"></p>
				</div>
				<form id="hangman" onsubmit="return false;">
					<input id="hangman-input" type="text">
					<input id="hangman-go" type="button" value="Los!">
				</form>
				<img id="hangman-lose" alt="Verloren" src="/img/hangmanlose.png">
			</section>
			
			<hr>
			
			<section id="password-part">
				<h2>Passwortjoko ♥</h2>
				<p>Banjoko möchte, dass deine Accounts sicher sind. Daher bietet Banjoko dir an sichere Passwörter zu generieren.</p>
				<form>
					<div class="one-line-container">
						<p>Passwortlänge (mind. 8):</p>
						<input id="password-length" type="number" value="8" min="8">					
					</div>
					<div class="one-line-container">
						<p>Anzahl Passwörter:</p>
						<input id="number-passwords" type="number" value="1" min="1"> 						
					</div>						
					<input id="generate-password" value="Generate!" type="button">	
				</form>
				<p id="password"></p>
			</section>
			
			<hr>
			
			<section id="psalm-part">
				<h2>Psalmjoko ♥</h2>
				<p>Dein Psalm, um Banjoko - unsere Herrin - gebührend zu ehren.</p>
				<p>
					<i id="psalm"><?php include("randompsalm.php"); ?></i>
				</p>
				<input id="copy-psalm" type="submit" value="Kopieren">
			</section>
			
			<hr>
			
			<section id="meme-gallery-part">
				<h2>Memejoko ♥</h2>
				<p>Rund um Banjoko ranken sich bereits etliche Legenden und Mythen.</p>
				<p>Dazu passend haben die zahlreichen Banjoko Jünger Memes erstellt, um an diese legendäre Geschehnisse zu erinnern.</p>
				<img class="meme" src="/img/meme1.png" alt="meme">
				<img class="meme" src="/img/meme2.png" alt="meme">
				<img class="meme" src="/img/meme3.png" alt="meme">
				<img class="meme" src="/img/meme4.png" alt="meme">
				<img class="meme" src="/img/meme5.png" alt="meme">
				<img class="meme" src="/img/meme6.png" alt="meme">
				<img class="meme" src="/img/meme7.png" alt="meme">
				<img class="meme" src="/img/meme8.png" alt="meme">
				<img class="meme" src="/img/meme9.png" alt="meme">
				<img class="meme" src="/img/meme10.png" alt="meme">
				<img class="meme" src="/img/meme11.png" alt="meme">
			</section>
		</main>		
		
		<aside id="cookies">
			<p>Mit der Benutzung dieser Seite akzeptierst du die Verwendung von Cookies. Cookies werden verwendet, um Banjoko zu füttern.</p>
			<input type="submit" value="Miau">
		</aside>
		
		<footer>
			<div>
				<a href="https://discord.gg/5ppaeEE" target="blank">Discord</a>
				<a href="https://gitlab.com/Timshi/website-banjoko" target="_blank">GitLab</a>
				<a href="mailto:incoming+timshi-website-banjoko-12100600-issue-@incoming.gitlab.com?subject=Bug%20report">Fehler melden</a>
			</div>
			<p id="created-text">created with ♥ by Azubi Excellence</p>
			<p>© Amsel-AP 2021</p>
		</footer>
	</body>
</html>
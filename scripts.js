var hashtags = null;
var winCases = [
	[true, true, true,
	 false, false, false,
	 false, false, false],
	
	[false, false, false,
	 true, true, true,
	 false, false, false],
	
	[false, false, false,
	 false, false, false,
	 true, true, true],
	
	[true, false, false,
	 true, false, false,
	 true, false, false],
	
	[false, true, false,
	 false, true, false,
	 false, true, false],
	
	[false, false, true,
	 false, false, true,
	 false, false, true],
	
	[true, false, false,
	 false, true, false,
	 false, false, true],
	
	[false, false, true,
	 false, true, false,
	 true, false, false]
];

window.onload = function() {
	// load JSON data
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
		hashtags = JSON.parse(this.responseText);
		
		initializeHangman();
	  }
	};
	request.open("REQUEST", "hashtags.php", true);
	request.send();
	
	// set event handlers
	id("restart").addEventListener("click", onRestartClick);
	id("hangman-go").addEventListener("click", hangmanOnClick);
	id("generate-password").addEventListener("click", onGeneratePasswort);
	id("copy-psalm").addEventListener("click", function() { copyInnerTextToClipboard("psalm") });
	id("password-length").addEventListener("focusout", onPasswordLengthInputFocusOut);
	document.querySelector("aside#cookies input[type=submit]").addEventListener("click", hideCookiesInfo);
	document.querySelectorAll("#ban-jo-ko .table-cell").forEach(e => {
		e.addEventListener("click", function() {
			onTicTacToeClick(e);
		})
	});
	
	// set random header picture
	id("header-image").src = "img/header" + Math.floor(Math.random() * 4) + ".png" 
}

var isGameFinished = false;
var isPlayerOneTurn = true;
var turn = 0;

function onTicTacToeClick(sender) {
	turn++;
	
	if (sender.children.length == 0 && !isGameFinished) {
		sender.appendChild(getPlayerMarker());
		
		if (checkWin()) {
			isGameFinished = true;
			id("winner").style.display = "block";
			var winnerText = id("winner-text");		
			
			if (isPlayerOneTurn) winnerText.innerHTML = "Player 1 wins!";
			else winnerText.innerHTML = "Player 2 wins!";
		} else if (turn === 9) {
			id("draw").style.display = "block";
		}
		
		isPlayerOneTurn = !isPlayerOneTurn;
	}
}

function checkWin() {
	var current = [
		isFieldChecked(1), isFieldChecked(2), isFieldChecked(3), 
		isFieldChecked(4), isFieldChecked(5), isFieldChecked(6), 
		isFieldChecked(7), isFieldChecked(8), isFieldChecked(9)
	];
	
	var result = false;
	
	for (i = 0; i < winCases.length; i++) {
		winCase = winCases[i];
		var isMatching = true;
		
		for (y = 0; y < winCase.length; y++) {
			if (winCase[y] !== current[y] && winCase[y] === true) {
				isMatching = false;
				continue;
			}
		}
		
		if (isMatching) {
			result = true;
			break;
		}
	}
	
	return result;
}

function getPlayerMarker() {
	var pic = document.createElement("img");
	pic.src = isPlayerOneTurn ? "img/player1.png" : "img/player2.png";	
	pic.height = 75;
	pic.width = 75;
	pic.style.borderRadius = "50%";
	
	return pic;
}

function isFieldChecked(elementId) {
	var children = id(elementId).children;
	for (i = 0; i < children.length; i++) {		
		if (children[i].src.includes(getPlayerMarker().src)) return true;
	}
	
	return false;
}

function onRestartClick() {
	location.reload();
}

var searchedWord;
var countLive = 7;
var failedGuesses = [];

// Cheater haben einen kleinen
function initializeHangman() {
	searchedWord = getRandomHashtag().replace("#", "");
	var text = "";
	for (i = 0; i < searchedWord.length; i++) text = text + "_";		
	id("searched-word").innerHTML = text;
}

// Cheater haben einen kleinen
function hangmanOnClick() {
	var guess = id("hangman-input").value;
		
	if (countLive > 0 && guess !== "") {
		var isMatching = false;
		
		// detect indexes to replace the _ with the entered letter
		for (i = 0; i < searchedWord.length; i++) {
			var letter = searchedWord[i];
			if (letter.toLowerCase() === guess.toLowerCase()) {
				isMatching = true;
				id("searched-word").innerHTML = replaceAt(id("searched-word").innerHTML, i, letter);
			}
		}
		
		// display/update the failed guesses and update count live and 
		if (!isMatching && countLive > 0) {			
			if (failedGuesses.includes(guess) === false) {
				id("failed-guesses-container").style.display = "block";
				failedGuesses[failedGuesses.length] = guess;
				id("failed-guesses").innerHTML = failedGuesses.join(", ");
			}
			
			countLive--;			
			if (countLive === 0) {
				id("live-counter").innerHTML = "RIP";
				id("hangman-lose").style.display = "block";
			}
			else {			
				id("live-counter").innerHTML = countLive;
			}
		}
	}
	
	id("hangman-input").value = "";
}

function onGeneratePasswort() {
	var numberOfPasswords = id("number-passwords").value;
	
	var text = "";
	for (j = 0; j < numberOfPasswords; j++) {
		text += getRandomPassword(id("password-length").value) + "<br>";
	}
	
	var password = id("password");
	password.style.display = "block";
	password.innerHTML = text;
	console.log("===================");
}

var letters = "abcdefghijklmopqrstuvwxyz";
var specialChars = [ "&excl;", "&bsol;", "&num;", "&dollar;", "&quot;", "&percnt;", "&amp;", "&apos;", "&lpar;", "&rpar;", "&ast;", "&add;", "&comma;", "&minus;", ".", "&sol;", "&colon;", "&semi;", "&lt;", "&equal;", "&rt;", "&s;", "&commat;", "&lsqb;", "&rsqb;", "&Hat;", "&lowbar;", "&grave;", "&lcub;", "&rcub;", "&verbar;" ]

function getRandomPassword(length) {
	var amount = length * 0.25;	
	var tmp = "";
	
	for (i = 0; i < amount; i++) {
		tmp += letters[getRandomInteger(letters.length - 1)];
		tmp += getRandomInteger(9);
		tmp += letters[getRandomInteger(letters.length - 1)].toUpperCase();
		tmp += specialChars[getRandomInteger(specialChars.length - 1)];
	}
	
	var usedIndexes = [];
	var result = "";
	
	for	(i = 0; i < length; i++) {
		var index = getRandomInteger(length - 1);
		while (usedIndexes.includes(index)) {
			index = getRandomInteger(length - 1);
		}
		
		result += tmp[index];
		usedIndexes[usedIndexes.length] = index;
	}
	
	console.log("Generated random password: " + result);
	return result;
}

function onPasswordLengthInputFocusOut() {
	var lengthInput = id("password-length");	
	if (lengthInput.value < 8) {
		lengthInput.value = 8;
	}
}

function getRandomInteger(maxValue) {
	return Math.floor(Math.random() * (maxValue + 1)); // maxValue + 1 because Math.random() returns 0 - 1 (0 inclusive, 1 not)
} 

function getRandomHashtag() {
	var index = getRandomInteger(hashtags.length - 1);
	return "#" + hashtags[index];
}

function hideCookiesInfo() {
	id("cookies").style.display = "none";
}

function id(id) {
	return document.getElementById(id);
}

function replaceAt(string, index, replacement) {
    return string.substr(0, index) + replacement + string.substr(index + replacement.length);
}

function copyInnerTextToClipboard(elementId) {	
	textArea = document.createElement('textarea');
	textArea.value = "\"" + id(elementId).innerHTML + "\"" + "\r\n- Mein Psalm von https://banjoko.love/";
	document.body.appendChild(textArea);
	textArea.select();
	document.execCommand('copy');
	document.body.removeChild(textArea);
}
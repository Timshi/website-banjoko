<?php
    include_once("sqlconfig.php");

    $servername = "localhost";
    
    $conn = new mysqli($servername, USERNAME, PASSWORD);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $result = $conn->query("SELECT psalm FROM banjoko.psalms ORDER BY RAND() LIMIT 1;")->fetch_assoc();
    echo $result["psalm"];

    $conn->close();
?>